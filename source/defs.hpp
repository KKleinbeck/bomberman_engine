#pragma once
//------------------------------------------------
//  DEFS.HPP
//------------------------------------------------

#ifndef DEFS_HPP
#define DEFS_HPP

#include <vector>

// Communication flags definitions
#define MESSAGE_INITIAL_CONTACT			0
#define MESSAGE_CONNECTION_ESTABLISHED	1
#define MESSAGE_GAMEBOARD_UPDATE		2
#define MESSAGE_PLAYER_ACTION			3
#define MESSAGE_KILLCOMMAND				4
#define NOMESSAGE						5
#define CONNECTION_TERMINATED			6

// Iterator definitions and name shortcuts
enum class states 			{ EMPTY, PLAYER_ONE, PLAYER_TWO, INDESTRUCTABLE, DESCTRUCTABLE,
	DESCTRUCTABLE_TELEPORTER, DESCTRUCTABLE_POWERUP_RANGE, DESCTRUCTABLE_POWERUP_AMOUNT,
	DESCTRUCTABLE_POWERUP_SPEED, POWERUP_RANGE, POWERUP_AMOUNT, POWERUP_SPEED, TIMED_BOMB_PLAYER_ONE,
	TIMED_BOMB_PLAYER_TWO, TRIGGERED_BOMB_PLAYER_ONE, TRIGGERED_BOMB_PLAYER_TWO, PLAYER_ONE_ON_TIMED_BOMB,
	PLAYER_TWO_ON_TIMED_BOMB, PLAYER_ONE_ON_TRIGGERED_BOMB, PLAYER_TWO_ON_TRIGGERED_BOMB, TELEPORTER };
typedef std::vector<std::vector<states>> tiles;

enum class actions			{ NONE, LEFT, RIGHT, UP, DOWN, PLACE_TIMED_BOMB, PLACE_TRIGGERED_BOMB,
	TRIGGER_BOMBS };

enum class output_states 	{ EMPTY, PLAYER_ONE, PLAYER_TWO, INDESTRUCTABLE, DESCTRUCTABLE,
	POWERUP_RANGE, POWERUP_AMOUNT, POWERUP_SPEED, TIMED_BOMB_PLAYER_ONE, TIMED_BOMB_PLAYER_TWO,
	TRIGGERED_BOMB_PLAYER_ONE, TRIGGERED_BOMB_PLAYER_TWO, PLAYER_ONE_ON_TIMED_BOMB, PLAYER_TWO_ON_TIMED_BOMB,
	PLAYER_ONE_ON_TRIGGERED_BOMB, PLAYER_TWO_ON_TRIGGERED_BOMB, TELEPORTER };

#endif
