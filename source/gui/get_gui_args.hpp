#pragma once
//------------------------------------------------
//  GET_GUI_ARGS.HPP
//------------------------------------------------

#ifndef GET_GUI_ARGS_HPP
#define GET_GUI_ARGS_HPP

int read_parameters(int argc, char* argv[], unsigned &port, bool &with_controls);

#endif
