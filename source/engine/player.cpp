//------------------------------------------------
// PLAYER.CPP
//------------------------------------------------

#include "player.hpp"
#include <map>

// Libraries

// CODE
std::map<states, it_power_up_state> states_to_powerup_cast =
{
	{states::POWERUP_RANGE, it_power_up_state::RANGE},
	{states::POWERUP_SPEED, it_power_up_state::SPEED},
	{states::POWERUP_AMOUNT, it_power_up_state::AMOUNT}
};

// Constructors

// Methods
void player::give_power_up(states power_up_index)
{
	if(power_up_index == states::POWERUP_RANGE && _power_up_state & it_power_up_state::RANGE)
	{
		_power_up_state = _power_up_state | it_power_up_state::RANGE_2;
	}
	else
	{
		_power_up_state = _power_up_state | states_to_powerup_cast[power_up_index];
	}
}

void player::set_into_motion()
{
	if(_power_up_state & it_power_up_state::SPEED)
	{
		_in_motion = 4;
	}
	else
	{
		_in_motion = 7;
	}
}

bool player::can_place_bomb()
{
	// The flag it_power_up_state::AMOUNT has the numerical value 2
	// Therefore this allows for two additional bombs. This hack would not work for other values
	return (_placed_bombs < 2 + (_power_up_state & it_power_up_state::AMOUNT) );
}
