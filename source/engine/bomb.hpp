#pragma once
//------------------------------------------------
//  BOMB.HPP
//------------------------------------------------

#ifndef BOMB_HPP
#define BOMB_HPP

// Libraries
#include <vector>
#include <string>

// My Libraries
#include "engine_defs.hpp"
#include "player.hpp"

// Typedefs and Structures
struct bomb_handler
{
	coordinates coord;
	bool player_id;
	int count_down;

	bomb_handler(coordinates c_in, bool id, int cd_in = 30) : coord(c_in), player_id(id), count_down(cd_in) {}
};

#endif
