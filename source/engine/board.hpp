
#pragma once
//------------------------------------------------
//  BOARD.HPP
//------------------------------------------------

#ifndef BOARD_HPP
#define BOARD_HPP

// Libraries

// My Libraries
#include "player.hpp"
#include "defs.hpp"
#include "engine_defs.hpp"

// board : holds the state of the gameboard, provides methods for loading board and set up duties
struct board
{
	private:
		bool _find_players(player &player_one, player &player_two);
		bool _place_teleporters(coordinates *teleporter_pos, player &player_one, player &player_two);
		bool _place_power_ups(const input_params &game_parameters, player &player_one, player &player_two);

	public:
		tiles state;
		int num_teleporters_found;

		board() : num_teleporters_found(0) {}

		void load_gameboard(const input_params &game_parameters, player &player_one, player &player_two,
				coordinates *teleporter_pos);
};


#endif
