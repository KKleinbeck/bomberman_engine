#pragma once
//------------------------------------------------
//  ENGINE_DEFS.HPP
//------------------------------------------------

#ifndef ENGINE_DEFS_HPP
#define ENGINE_DEFS_HPP

#include <string>

// Bomb Event flags definitions
#define BOMBEVENT_NO_HIT				0
#define BOMBEVENT_PLAYER_ONE_HIT		1
#define BOMBEVENT_PLAYER_TWO_HIT		2
#define BOMBEVENT_BOTH_HIT				3

// Iterator definitions and name shortcuts
enum it_power_up_state		{ NONE = 0, SPEED = 1, AMOUNT = 2, RANGE = 4, RANGE_2 = 8};

// Structures
struct coordinates
{
	unsigned x, y;

	coordinates(unsigned x_in = 0, unsigned y_in = 0) : x(x_in), y(y_in) {};

	unsigned distance_to(unsigned x_other, unsigned y_other)
	{
		return ( (x > x_other ? x - x_other : x_other - x)
				+ (y > y_other ? y - y_other : y_other - y) );
	}
};

struct rewards
{
	int moved,
		moved_bomb,
		teleported,
		deploy_timed_bomb,
		deploy_triggered_bomb,
		trigger_bomb,
		bomb_chain_explosion,
		destroy_destructible,
		pickup_powerup,
		destroy_usable_powerup,
		destroy_unusable_powerup,
		suicide,
		die,
		kill_opponent,
		opponent_suicided;
	
	rewards() : moved(0), moved_bomb(5), teleported(5), deploy_timed_bomb(1), deploy_triggered_bomb(1),
		trigger_bomb(1), bomb_chain_explosion(5), destroy_destructible(10), pickup_powerup(30),
		destroy_usable_powerup(-10), destroy_unusable_powerup(30), suicide(-100), die(-100), kill_opponent(200),
		opponent_suicided(50) {}
};

struct input_params
{
	bool use_teleporters;
	bool use_amount_upgrades;
	bool use_range_upgrades;
	bool use_speed_upgrades;

	bool use_gui, human_controls;

	std::string ai1_command, ai2_command, gui_command;

	std::string map, port_ai1, port_ai2, port_gui;

	rewards player_rewards;

	input_params() : use_teleporters(true), use_amount_upgrades(true), use_range_upgrades(true),
		use_speed_upgrades(true), use_gui(false), human_controls(false) {}
	// Both the ports' and the map's default value are set in the argument parser
};

#endif
