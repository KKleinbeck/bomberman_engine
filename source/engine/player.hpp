#pragma once
//------------------------------------------------
//  PLAYER.HPP
//------------------------------------------------

#ifndef PLAYER_HPP
#define PLAYER_HPP

// Libraries
#include <vector>
#include <string>

// My Libraries
#include "defs.hpp"
#include "engine_defs.hpp"

// player : stores player coordinates and upgrade Status
class  player
{
	private:
		char _power_up_state;
		int _score;
		int _in_motion;
		int _placed_bombs;
	
	public:
		coordinates position;
		// std::chrono::... action_down_time; Store time until which player is unmoveable
		
		player() : _power_up_state(it_power_up_state::NONE), _score(0), _in_motion(0),
			_placed_bombs(0), position(coordinates()) {}
		
		void give_power_up(states power_up_index);
		char get_power_up_state(void) {return _power_up_state;}

		void add_to_score(int change) {_score += change;}
		int get_score(void) {return _score;}

		void set_into_motion(void);
		void continue_motion(void) {--_in_motion;}
		bool is_in_motion(void) {return (_in_motion != 0);}

		bool can_place_bomb(void);
		void placed_bomb(void) {++_placed_bombs;}
		void gets_new_bomb(void) {--_placed_bombs;}
};

#endif
