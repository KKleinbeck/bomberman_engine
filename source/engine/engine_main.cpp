#include <iostream>
#include <ratio>
#include <zmq.hpp>
#include <unistd.h>
#include <iostream>

#include "engine_defs.hpp"
#include "get_engine_args.hpp"
#include "engine.hpp"
#include "client.hpp"
#include "communication.hpp"

#define MAX_GAME_TICKS 10000 // Play for 10000 "rounds" equalling 400 seconds in gui mode or 1 second in KI mode

int main(int argc, char* argv[])
{
	input_params game_parameters;
	if( read_parameters(argc, argv, game_parameters) == -1 )
	{
		return 1;
	}
	
	// create the gameboard
	engine *game_engine;
	try
	{
		game_engine = new engine(game_parameters);
	}
	catch(std::string &e)
	{
		std::cerr << e << std::endl;
		return -1;
	}
	char dim_x(game_engine->get_width()), dim_y(game_engine->get_height());
	tiles states(game_engine->get_board() );

	// create the server, spawn all clients and check if they have connected
	zmq::context_t context(1);
	std::size_t num_clients(2);
	if(game_parameters.use_gui == true && game_parameters.human_controls == false)
	{
		num_clients = 3;
	}
	client_handle *clients[num_clients];

	clients[0] = new client_handle(context, "AI1", 0, 1);
	clients[0]->open(game_parameters.port_ai1, game_parameters.ai1_command);

	if(game_parameters.use_gui == false)
	{
		clients[1] = new client_handle(context, "AI2", 1, 1);
		clients[1]->open(game_parameters.port_ai2, game_parameters.ai2_command);
	}
	else if(game_parameters.human_controls == false)
	{
		clients[1] = new client_handle(context, "AI2", 1, 1);
		clients[1]->open(game_parameters.port_ai2, game_parameters.ai2_command);
		clients[2] = new client_handle(context, "GUI", 0, 0);
		clients[2]->open(game_parameters.port_gui, game_parameters.gui_command);
	}
	else
	{
		clients[1] = new client_handle(context, "GUI", 1, 1);
		clients[1]->open(game_parameters.port_gui, game_parameters.gui_command);
	}

	try
	{
		for(const auto& client : clients)
		{
			if(establish_client_connection(client->socket, dim_x, dim_y, game_engine->get_board(),
						client->player_id) )
			{
				throw(std::string("Failed to establish a connection to the " + client->name));
			}
		}
	}
	catch(std::string error)
	{
		std::cout << error << std::endl;
		for(const auto& client : clients)
		{
			client->close();
			delete client;
		}

		delete game_engine;
		return -1;
	}

	// run the main loop
	actions player_action(actions::NONE);
	bool game_is_running(true);
	int idle_time(game_parameters.use_gui ? 40000 : 1000);
	int bombevent = BOMBEVENT_NO_HIT;
	for(int ticks = 0; ticks <= MAX_GAME_TICKS && game_is_running && !bombevent; ++ticks)
	{
		for(const auto& client : clients)
		{
			switch(recv_player_action(client->socket, player_action, client->player_id) )
			{
				case MESSAGE_PLAYER_ACTION:
					if(client->is_active)
					{
						bombevent = game_engine->player_action(player_action, client->player_id);
					}
					break;

				case CONNECTION_TERMINATED:
					std::cout << "Lost connection to the " + client->name + "; ending the game now." << std::endl;
					game_is_running = false;
					break;

				case MESSAGE_KILLCOMMAND:
					if(client->name == "GUI")
					{
						std::cout << "Kill command recived." << std::endl;
						game_is_running = false;
					}
					break;

				default:
					break;
			}

			send_gameboard(client->socket, dim_x, dim_y, game_engine->get_board(), client->player_id);
		}

		bombevent |= game_engine->update_game();

		usleep(idle_time);
	}

	// Process the game outcome
	switch(bombevent)
	{
		case BOMBEVENT_PLAYER_ONE_HIT:
			std::cout << "Player 1 died" << std::endl;
			break;

		case BOMBEVENT_PLAYER_TWO_HIT:
			std::cout << "Player 2 died" << std::endl;
			break;

		case BOMBEVENT_BOTH_HIT:
			std::cout << "Both player died" << std::endl;
			break;

		default:
			std::cout << "Game ended on time" << std::endl;
			break;
	}
	std::cout << game_engine->get_player_score(0) << "\t" << game_engine->get_player_score(1) << std::endl;

	// terminate every child process
	for(const auto& client : clients)
	{
		client->close();
		usleep(50);
		delete client;
	}

	delete game_engine;
	return 0;
}
