#include "communication.hpp"
#include "unistd.h"
#include "defs.hpp"

int main(int argc, char* argv[])
{
	// A structure with the mandatory command line arguments
	//mandatory_args args(extract_mandatory(argc, argv));

	// Connect to the engine
	zmq::context_t context(1);
	zmq::socket_t engine_socket(context, ZMQ_PAIR);

	engine_socket.connect("tcp://localhost:5555");

	char dim_x, dim_y;
	tiles gameboard;
	if(establish_server_connection(engine_socket, dim_x, dim_y, gameboard)) return -1;

	// Main Loop
	bool game_is_running(true);
	char command(0);
	while(game_is_running)
	{
		// Your code should go here
		send_player_action(engine_socket, actions::DOWN);
		usleep(1000);

		if( (recv_gameboard(engine_socket, gameboard, &command) && command != NOMESSAGE) ||
				command == MESSAGE_KILLCOMMAND )
		{
			game_is_running = false;
		}
	}

	return 0;
}
