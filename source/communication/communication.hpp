#pragma once
//------------------------------------------------
//  COMMUNICATION.HPP
//------------------------------------------------

#ifndef COMMUNICATION_HPP
#define COMMUNICATION_HPP

// Libraries
#include <zmq.hpp>

// My Libraries
#include "defs.hpp"
#include "engine_defs.hpp"

// Typedefs and Structures

// Basic function for the interprocess communication
bool establish_client_connection(zmq::socket_t &socket, char dim_x, char dim_y, tiles board, bool player_id);
bool establish_server_connection(zmq::socket_t &socket, char &dim_x, char &dim_y, tiles &board);

bool send_gameboard(zmq::socket_t &socket, char dim_x, char dim_y, tiles board, bool player_id);
bool recv_gameboard(zmq::socket_t &socket, tiles &board, char *command = nullptr);

bool send_player_action(zmq::socket_t &socket, actions action);
char recv_player_action(zmq::socket_t &socket, actions &action, bool player_id);

#endif
