//------------------------------------------------
// ENGINE.CPP
//------------------------------------------------

#include "engine.hpp"

// Libraries
#include <fstream>

// CODE

// Constructors
engine::engine(const input_params &game_parameters) : player_rewards(game_parameters.player_rewards)
{
	gameboard.load_gameboard(game_parameters, player_one, player_two, teleporter_pos);
}

// Methods
int engine::player_action(actions act, bool player_id)
{
	if(_validate_player_action(act, player_id))
	{
		return _perform_player_action(act, player_id);
	}
	return BOMBEVENT_NO_HIT;
}

int engine::_perform_player_action(actions act, bool player_id)
{
	if(act == actions::LEFT || act == actions::RIGHT || act == actions::UP || act == actions::DOWN)
	{
		return _perform_movement_action(act, player_id);
	}
	else
	{
		return _perform_bomb_action(act, player_id);
	}
}

int engine::_perform_movement_action(actions act, bool player_id)
{
	player &current_player( !player_id ? player_one : player_two );
	states player_state( !player_id ? states::PLAYER_ONE : states::PLAYER_TWO ),
		PLAYER_ON_TIMED_BOMB( !player_id ? states::PLAYER_ONE_ON_TIMED_BOMB : states::PLAYER_TWO_ON_TIMED_BOMB),
		PLAYER_ON_TRIGGERED_BOMB( !player_id ? states::PLAYER_ONE_ON_TRIGGERED_BOMB
				: states::PLAYER_TWO_ON_TRIGGERED_BOMB);

	unsigned &x(current_player.position.x), &y(current_player.position.y);

	// Assume player moved, set old position's tile accordingly
	if(gameboard.state[y][x] == PLAYER_ON_TIMED_BOMB)
	{
		gameboard.state[y][x] = (!player_id ? states::TIMED_BOMB_PLAYER_ONE : states::TIMED_BOMB_PLAYER_TWO);
	}
	else if(gameboard.state[y][x] == PLAYER_ON_TRIGGERED_BOMB)
	{
		gameboard.state[y][x] = (!player_id ? states::TRIGGERED_BOMB_PLAYER_ONE : states::TRIGGERED_BOMB_PLAYER_TWO);
	}
	else
	{
		gameboard.state[y][x] = states::EMPTY;
	}

	// perform the players action
	int x_change(0), y_change(0);
	switch(act)
	{
		case actions::LEFT:
			x_change = -1;
			break;

		case actions::UP:
			y_change = -1;
			break;

		case actions::RIGHT:
			x_change = 1;
			break;

		case actions::DOWN:
			y_change = 1;
			break;

		default:
			break;
	}

	current_player.set_into_motion();
	if(y + y_change == teleporter_pos[0].y && x + x_change == teleporter_pos[0].x )
	{
		y = teleporter_pos[1].y + y_change;
		x = teleporter_pos[1].x + x_change;
		current_player.add_to_score(player_rewards.teleported);
	}
	else if(y + y_change == teleporter_pos[1].y && x + x_change == teleporter_pos[1].x )
	{
		y = teleporter_pos[0].y + y_change;
		x = teleporter_pos[0].x + x_change;
		current_player.add_to_score(player_rewards.teleported);
	}
	else
	{
		x += x_change;
		y += y_change;

		// test, whether player stands on a power up; pick up power up
		// or alternatively the player moves a bomb
		if(gameboard.state[y][x] == states::POWERUP_RANGE || gameboard.state[y][x] == states::POWERUP_SPEED
				|| gameboard.state[y][x] == states::POWERUP_AMOUNT)
		{
			current_player.give_power_up(gameboard.state[y][x]);
			current_player.add_to_score(player_rewards.pickup_powerup);
		}
		else if(gameboard.state[y][x] == states::TIMED_BOMB_PLAYER_ONE ||
				gameboard.state[y][x] == states::TIMED_BOMB_PLAYER_TWO ||
				gameboard.state[y][x] == states::TRIGGERED_BOMB_PLAYER_ONE ||
				gameboard.state[y][x] == states::TRIGGERED_BOMB_PLAYER_TWO )
		{
			_move_bomb(x, y, x_change, y_change);
			current_player.add_to_score(player_rewards.moved_bomb);
		}
		else
		{
			current_player.add_to_score(player_rewards.moved);
		}
	}

	gameboard.state[y][x] = player_state;

	return BOMBEVENT_NO_HIT;
}

int engine::_perform_bomb_action(actions act, bool player_id)
{
	player &current_player( !player_id ? player_one : player_two );

	unsigned &x(current_player.position.x), &y(current_player.position.y);

	switch(act)
	{
		case actions::PLACE_TIMED_BOMB:
			gameboard.state[y][x] = ( !player_id ? states::PLAYER_ONE_ON_TIMED_BOMB
				: states::PLAYER_TWO_ON_TIMED_BOMB);
			timed_bombs.push_back(bomb_handler( coordinates(x, y), player_id) );
			current_player.add_to_score(player_rewards.deploy_timed_bomb);
			current_player.placed_bomb();
			break;

		case actions::PLACE_TRIGGERED_BOMB:
			gameboard.state[y][x] = ( !player_id ? states::PLAYER_ONE_ON_TRIGGERED_BOMB
				: states::PLAYER_TWO_ON_TRIGGERED_BOMB);
			triggered_bombs[player_id].push_back(bomb_handler( coordinates(x, y), player_id, -1) );
			current_player.add_to_score(player_rewards.deploy_triggered_bomb);
			current_player.placed_bomb();
			break;

		case actions::TRIGGER_BOMBS:
		{
			current_player.add_to_score(player_rewards.deploy_triggered_bomb);
			int bombevent = BOMBEVENT_NO_HIT;
			for(auto& bomb : triggered_bombs[player_id]) bombevent |= _detonate_bomb(bomb);
			triggered_bombs[player_id].clear();
			return bombevent;
		}

		default:
			break;
	}

	return BOMBEVENT_NO_HIT;
}

void engine::_move_bomb(int x, int y, int x_change, int y_change)
{
	states bomb_type(gameboard.state[y][x]);
	unsigned new_x, new_y;
	if(gameboard.state[y+y_change][x+x_change] == states::EMPTY)
	{
		new_x = x + x_change;
		new_y = y + y_change;
		gameboard.state[new_y][new_x] = bomb_type;
	}
	else if(x + x_change == (int) teleporter_pos[0].x && y + y_change == (int) teleporter_pos[0].y)
	{
		new_x = teleporter_pos[1].x + x_change;
		new_y = teleporter_pos[1].y + y_change;
		gameboard.state[new_y][new_x] = bomb_type;
	}
	else if(x + x_change == (int) teleporter_pos[1].x && y + y_change == (int) teleporter_pos[1].y)
	{
		new_x = teleporter_pos[0].x + x_change;
		new_y = teleporter_pos[0].y + y_change;
		gameboard.state[new_y][new_x] = bomb_type;
	}

	if(bomb_type == states::TIMED_BOMB_PLAYER_ONE || bomb_type == states::TIMED_BOMB_PLAYER_TWO)
	{
		for(auto &timed_bomb : timed_bombs)
		{
			if((int) timed_bomb.coord.x == x && (int) timed_bomb.coord.y == y)
			{
				timed_bomb.coord.x = new_x;
				timed_bomb.coord.y = new_y;
				break;
			}
		}
	}
	else
	{
		for(auto &triggered_bomb : triggered_bombs[bomb_type != states::TRIGGERED_BOMB_PLAYER_ONE])
		{
			if((int) triggered_bomb.coord.x == x && (int) triggered_bomb.coord.y == y)
			{
				triggered_bomb.coord.x = new_x;
				triggered_bomb.coord.y = new_y;
				break;
			}
		}
	}
}

bool engine::_validate_player_action(actions act, bool player_id)
{
	player &current_player( !player_id ? player_one : player_two );
	int x(current_player.position.x), y(current_player.position.y),
		x_change(0), y_change(0);

	if(current_player.is_in_motion() ) return 0;

	switch(act)
	{
		case actions::LEFT:
			x_change = -1;
			break;

		case actions::UP:
			y_change = -1;
			break;

		case actions::RIGHT:
			x_change = 1;
			break;

		case actions::DOWN:
			y_change = 1;
			break;

		case actions::PLACE_TIMED_BOMB:
			if(gameboard.state[y][x] == states::PLAYER_ONE_ON_TIMED_BOMB ||
				gameboard.state[y][x] == states::PLAYER_TWO_ON_TIMED_BOMB ||
				gameboard.state[y][x] == states::PLAYER_ONE_ON_TRIGGERED_BOMB ||
				gameboard.state[y][x] == states::PLAYER_TWO_ON_TRIGGERED_BOMB	||
				!current_player.can_place_bomb() )
			{
				return 0;
			}
			return 1;

		case actions::PLACE_TRIGGERED_BOMB:
			if(gameboard.state[y][x] == states::PLAYER_ONE_ON_TIMED_BOMB ||
				gameboard.state[y][x] == states::PLAYER_TWO_ON_TIMED_BOMB ||
				gameboard.state[y][x] == states::PLAYER_ONE_ON_TRIGGERED_BOMB ||
				gameboard.state[y][x] == states::PLAYER_TWO_ON_TRIGGERED_BOMB	||
				!current_player.can_place_bomb() )
			{
				return 0;
			}
			return 1;

		case actions::TRIGGER_BOMBS:
			return 1;

		case actions::NONE:
			return 0;
	}

	// If we did not exit yet, a movement action was performed. We check it here instead of in the switch
	// statement, so that we can treat every case together
	int new_x(x + x_change), new_y(y + y_change);
	if(new_x < 0 || new_y < 0 || new_x == (int) get_width() || new_y == (int) get_height() )
	{
		return 0;
	}

	if((int) teleporter_pos[0].x == new_x && (int) teleporter_pos[0].y == new_y &&
		gameboard.num_teleporters_found == 2)
	{
		new_x = teleporter_pos[1].x + x_change;
		new_y = teleporter_pos[1].y + y_change;
	}
	else if((int) teleporter_pos[1].x == new_x && (int) teleporter_pos[1].y == new_y &&
		gameboard.num_teleporters_found == 2)
	{
		new_x = teleporter_pos[0].x + x_change;
		new_y = teleporter_pos[0].y + y_change;
	}

	if(gameboard.state[new_y][new_x] == states::EMPTY ||
		gameboard.state[new_y][new_x] == states::POWERUP_RANGE ||
		gameboard.state[new_y][new_x] == states::POWERUP_SPEED ||
		gameboard.state[new_y][new_x] == states::POWERUP_AMOUNT)
	{
		return 1;
	}

	// Lastly, check if we are walking towards a bomb, in which case we want to push the bomb in the
	// direction
	if(gameboard.state[new_y][new_x] == states::TRIGGERED_BOMB_PLAYER_ONE ||
		gameboard.state[new_y][new_x] == states::TRIGGERED_BOMB_PLAYER_TWO ||
		gameboard.state[new_y][new_x] == states::TIMED_BOMB_PLAYER_ONE ||
		gameboard.state[new_y][new_x] == states::TIMED_BOMB_PLAYER_TWO)
	{
		int x_change(new_x - x), y_change(new_y - y);

		if(new_x + x_change < 0 || new_y + y_change < 0 ||
			new_x + x_change == (int) get_width() || new_y + y_change == (int) get_height() )
		{
			return 0;
		}

		if(gameboard.state[new_y + y_change][new_x + x_change] == states::EMPTY ||
			( gameboard.num_teleporters_found == 2 && (
				(new_x + x_change == (int) teleporter_pos[0].x && 
				 new_y + y_change == (int) teleporter_pos[0].y &&
				 gameboard.state[teleporter_pos[1].y + y_change][teleporter_pos[1].x + x_change] == states::EMPTY ) ||
				(new_x + x_change == (int) teleporter_pos[1].x && new_y + y_change == (int) teleporter_pos[1].y &&
				 gameboard.state[teleporter_pos[0].y + y_change][teleporter_pos[0].x + x_change] == states::EMPTY ) )
			)
		  )
		{
			return 1;
		}
	}


	return 0;
}

int engine::update_game()
{
	if(player_one.is_in_motion() )
	{
		player_one.continue_motion();
	}
	if(player_two.is_in_motion() )
	{
		player_two.continue_motion();
	}

	return _update_bombs();
}

int engine::_update_bombs()
{
	for(auto& bomb : timed_bombs) --bomb.count_down;

	// After performing the bombing actions, clean up exploded bombs
	int event(BOMBEVENT_NO_HIT);
	while(timed_bombs.size() > 0 && timed_bombs[0].count_down == 0)
	{
		event |= _detonate_bomb(timed_bombs[0]);
		timed_bombs.erase(timed_bombs.begin() );
	}
	
	return event;
}

int engine::_detonate_bomb(bomb_handler &bomb, int player_id)
{
	if(player_id == -1) player_id = (int) bomb.player_id;
	player &current_player(!player_id ? player_one : player_two);
	current_player.gets_new_bomb();

	int event(BOMBEVENT_NO_HIT);

	// First check if the player still stands on the bomb
	switch(gameboard.state[bomb.coord.y][bomb.coord.x])
	{
		case states::PLAYER_ONE_ON_TIMED_BOMB:
		case states::PLAYER_ONE_ON_TRIGGERED_BOMB:
			event = event | BOMBEVENT_PLAYER_ONE_HIT;
			player_one.add_to_score(player_rewards.suicide);
			player_two.add_to_score(player_rewards.opponent_suicided);
			break;

		case states::PLAYER_TWO_ON_TIMED_BOMB:
		case states::PLAYER_TWO_ON_TRIGGERED_BOMB:
			event = event | BOMBEVENT_PLAYER_TWO_HIT;
			player_two.add_to_score(player_rewards.suicide);
			player_one.add_to_score(player_rewards.opponent_suicided);
			break;

		default:
			break;
	}
	gameboard.state[bomb.coord.y][bomb.coord.x] = states::EMPTY;

	// Now check in explosion direction
	char range, powerup_state;

	powerup_state = current_player.get_power_up_state();
	range = 1 + (powerup_state >= it_power_up_state::RANGE) + (powerup_state >= it_power_up_state::RANGE_2);

	std::vector<coordinates> directions(4);
	directions[0] = coordinates( 1, 0);
	directions[1] = coordinates(-1, 0);
	directions[2] = coordinates( 0, 1);
	directions[3] = coordinates( 0,-1);

	coordinates current_coord;
	states tile_after_destruction;
	for(coordinates &direction : directions)
	{
		for(int i = 1; i <= range; ++i)
		{
			current_coord.x = bomb.coord.x + i*direction.x;
			current_coord.y = bomb.coord.y + i*direction.y;
			if(0 > current_coord.x || current_coord.x >= this->get_width()
					|| 0 > current_coord.y || current_coord.y >= this->get_height()
					|| gameboard.state[current_coord.y][current_coord.x] == states::INDESTRUCTABLE
					|| gameboard.state[current_coord.y][current_coord.x] == states::TELEPORTER)
			{
				break;
			}

			tile_after_destruction = states::EMPTY;
			bool other_bomb_hit(false);
			switch (gameboard.state[current_coord.y][current_coord.x])
			{
				case states::DESCTRUCTABLE:
					current_player.add_to_score(player_rewards.destroy_destructible);
					break;

				case states::POWERUP_RANGE:
					if(powerup_state & it_power_up_state::RANGE_2)
					{
						current_player.add_to_score(player_rewards.destroy_unusable_powerup);
					}
					else
					{
						current_player.add_to_score(player_rewards.destroy_usable_powerup);
					}
					break;

				case states::POWERUP_AMOUNT:
					if(powerup_state & it_power_up_state::AMOUNT)
					{
						current_player.add_to_score(player_rewards.destroy_unusable_powerup);
					}
					else
					{
						current_player.add_to_score(player_rewards.destroy_usable_powerup);
					}
					break;

				case states::POWERUP_SPEED:
					if(powerup_state & it_power_up_state::SPEED)
					{
						current_player.add_to_score(player_rewards.destroy_unusable_powerup);
					}
					else
					{
						current_player.add_to_score(player_rewards.destroy_usable_powerup);
					}
					break;

				case states::PLAYER_ONE_ON_TIMED_BOMB:
				case states::PLAYER_ONE_ON_TRIGGERED_BOMB:
					other_bomb_hit = true;
				case states::PLAYER_ONE:
					event |= BOMBEVENT_PLAYER_ONE_HIT;
					if(bomb.player_id == 0)
					{
						player_one.add_to_score(player_rewards.suicide);
						player_two.add_to_score(player_rewards.opponent_suicided);
					}
					else
					{
						player_one.add_to_score(player_rewards.die);
						player_two.add_to_score(player_rewards.kill_opponent);
					}
					break;

				case states::PLAYER_TWO_ON_TIMED_BOMB:
				case states::PLAYER_TWO_ON_TRIGGERED_BOMB:
					other_bomb_hit = true;
				case states::PLAYER_TWO:
					event |= BOMBEVENT_PLAYER_TWO_HIT;
					if(bomb.player_id == 1)
					{
						player_two.add_to_score(player_rewards.suicide);
						player_one.add_to_score(player_rewards.opponent_suicided);
					}
					else
					{
						player_two.add_to_score(player_rewards.die);
						player_one.add_to_score(player_rewards.kill_opponent);
					}
					break;

				case states::TIMED_BOMB_PLAYER_ONE:
				case states::TIMED_BOMB_PLAYER_TWO:
				case states::TRIGGERED_BOMB_PLAYER_ONE:
				case states::TRIGGERED_BOMB_PLAYER_TWO:
					tile_after_destruction = gameboard.state[current_coord.y][current_coord.x];
					other_bomb_hit = true;
					break;

				case states::DESCTRUCTABLE_TELEPORTER:
					tile_after_destruction = states::TELEPORTER;
					gameboard.num_teleporters_found++;
					current_player.add_to_score(player_rewards.destroy_destructible);
					break;

				case states::DESCTRUCTABLE_POWERUP_RANGE:
					tile_after_destruction = states::POWERUP_RANGE;
					current_player.add_to_score(player_rewards.destroy_destructible);
					break;

				case states::DESCTRUCTABLE_POWERUP_AMOUNT:
					tile_after_destruction = states::POWERUP_AMOUNT;
					current_player.add_to_score(player_rewards.destroy_destructible);
					break;

				case states::DESCTRUCTABLE_POWERUP_SPEED:
					tile_after_destruction = states::POWERUP_SPEED;
					current_player.add_to_score(player_rewards.destroy_destructible);
					break;

				default:
					break;
			}
			gameboard.state[current_coord.y][current_coord.x] = tile_after_destruction;

			if(other_bomb_hit)
			{
				event |= _detonate_bomb_at(current_coord.x, current_coord.y, player_id);
			}
		}
	}

	return event;
}

int engine::_detonate_bomb_at(unsigned x, unsigned y, int player_id)
{
	int event(BOMBEVENT_NO_HIT);

	for(auto bomb_list : {timed_bombs, triggered_bombs[0], triggered_bombs[1]})
	{
		for(unsigned i = 0; i < bomb_list.size(); ++i)
		{
			if(bomb_list[i].coord.x == x && bomb_list[i].coord.y == y)
			{
				event = _detonate_bomb(bomb_list[i], player_id);
				bomb_list.erase(bomb_list.begin() + 1);
			}
		}
	}

	return event;
}
