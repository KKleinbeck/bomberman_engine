//------------------------------------------------
// GUI.CPP
//------------------------------------------------

#include "gui.hpp"

// Libraries
#include <string>
#include <unistd.h>
#include <sstream>
#include <map>

#include "communication.hpp"

// CODE
std::map<states, std::string> tile_file =
{
	{ states::EMPTY,						"res/field.png"},
	{ states::INDESTRUCTABLE,				"res/solid.png"},
	{ states::DESCTRUCTABLE,				"res/destructable.png"},
	{ states::POWERUP_RANGE,				"res/range.png"},
	{ states::POWERUP_AMOUNT,				"res/amount.png"},
	{ states::POWERUP_SPEED,				"res/speed.png"},
	{ states::PLAYER_ONE,					"res/player1.png"},
	{ states::PLAYER_TWO,					"res/player2.png"},
	{ states::TIMED_BOMB_PLAYER_ONE,		"res/bombplayer1.png"},
	{ states::TIMED_BOMB_PLAYER_TWO,		"res/bombplayer2.png"},
	{ states::TRIGGERED_BOMB_PLAYER_ONE,	"res/triggeredplayer1.png"},
	{ states::TRIGGERED_BOMB_PLAYER_TWO,	"res/triggeredplayer2.png"},
	{ states::PLAYER_ONE_ON_TIMED_BOMB,		"res/player1onbomb.png"},
	{ states::PLAYER_TWO_ON_TIMED_BOMB,		"res/player2onbomb.png"},
	{ states::PLAYER_ONE_ON_TRIGGERED_BOMB,	"res/player1ontriggered.png"},
	{ states::PLAYER_TWO_ON_TRIGGERED_BOMB,	"res/player2ontriggered.png"},
	{ states::DESCTRUCTABLE_TELEPORTER,		"res/destructable.png"},
	{ states::DESCTRUCTABLE_POWERUP_RANGE,	"res/destructable.png"},
	{ states::DESCTRUCTABLE_POWERUP_AMOUNT,	"res/destructable.png"},
	{ states::DESCTRUCTABLE_POWERUP_SPEED,	"res/destructable.png"},
	{ states::TELEPORTER,					"res/teleporter.png"}
};

// DrawPanel
BEGIN_EVENT_TABLE(DrawPanel, wxPanel)
EVT_PAINT(DrawPanel::paintEvent)
EVT_SIZE(DrawPanel::onSize)
EVT_KEY_DOWN(DrawPanel::onKeyDown)
END_EVENT_TABLE()

DrawPanel::DrawPanel(wxFrame *in_parent, char in_dim_x, char in_dim_y, tiles initial_state)
	: wxPanel(in_parent), dim_x(in_dim_x), dim_y(in_dim_y),
	preloaded_tiles(std::vector<wxImage>(tile_file.size())), parent(in_parent),
	tiles_state(initial_state), tiles_bitmap(dim_y, std::vector<wxBitmap>(dim_x)),
	latest_player_action(actions::NONE)
{
	// Create an instance of every possible pane, such that they dont need to get reloaded for every render call
	for(unsigned i = 0; i < tile_file.size(); ++i)
	{
		preloaded_tiles[i].LoadFile(tile_file[(states) i], wxBITMAP_TYPE_PNG);
		assert(preloaded_tiles[i].IsOk() );
	}
	tile_width = -1;
	tile_height = -1;

	//Connect(wxEVT_KEY_DOWN, wxCommandEventHandler(DrawPanel::onChar));
}
 
void DrawPanel::paintEvent(wxPaintEvent & evt)
{
    wxPaintDC dc(this);
    render(dc);
}

void DrawPanel::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}
 
void DrawPanel::render(wxDC&  dc)
{
	dc.Clear();

    int new_width, new_heigth;
    dc.GetSize(&new_width, &new_heigth);
 
    if( (new_width/dim_x != tile_width || new_heigth/dim_y != tile_height)
			&& new_width/dim_x > 0 && new_heigth/dim_y > 0 )
    {
        tile_width = new_width/dim_x;
        tile_height = new_heigth/dim_y;
		assigneBitmapToTiles(tile_width, tile_height);
    }
	for(int y = 0; y < dim_y; ++y)
	{
		for(int x = 0; x < dim_x; ++x)
		{
			dc.DrawBitmap(tiles_bitmap[y][x], tile_width*x, tile_height*y);
		}
	}
}

void DrawPanel::onSize(wxSizeEvent& event)
{
    Refresh();
    event.Skip();
}

void DrawPanel::assigneBitmapToTiles(int width, int height)
{
	for(int y = 0; y < dim_y; ++y)
	{
		for(int x = 0; x < dim_x; ++x)
		{
			tiles_bitmap[y][x] = wxBitmap( preloaded_tiles[(unsigned) tiles_state[y][x] ].Scale(width, height));
		}
	}
}

void DrawPanel::setTilesState(tiles in_state)
{
	tiles_state = in_state;
	assigneBitmapToTiles(tile_width, tile_height);
}

void DrawPanel::onKeyDown(wxKeyEvent &evt)
{
	wxString key_pressed(evt.GetUnicodeKey() );
	
	if(key_pressed == "W")
	{
		latest_player_action = actions::UP;
	}
	else if(key_pressed == "A")
	{
		latest_player_action = actions::LEFT;
	}
	else if(key_pressed == "S")
	{
		latest_player_action = actions::DOWN;
	}
	else if(key_pressed == "D")
	{
		latest_player_action = actions::RIGHT;
	}
	else if(key_pressed == "B")
	{
		latest_player_action = actions::PLACE_TIMED_BOMB;
	}
	else if(key_pressed == "T")
	{
		latest_player_action = actions::PLACE_TRIGGERED_BOMB;
	}
	else if(key_pressed == "Y")
	{
		latest_player_action = actions::TRIGGER_BOMBS;
	}
	//else if(key_pressed == "K")
	//{
		//latest_player_action = actions::TRIGGER_BOMBS;
	//}
	else
	{
		latest_player_action = actions::NONE;
	}
}

void DrawPanel::Close(bool force)
{
	parent->Close(force);
}


// wxMainLoop
wxMainLoop::wxMainLoop(char in_dim_x, char in_dim_y, zmq::socket_t &socket, bool controls, DrawPanel *in_panel)
	: wxTimer(), dim_x(in_dim_x), dim_y(in_dim_y), with_controls(controls), panel(in_panel),
	engine_socket(socket), tiles_state(dim_y, std::vector<states>(dim_x))
{
}
 
void wxMainLoop::Notify()
{
	char command(0);
	recv_gameboard(engine_socket, tiles_state, &command);
	if(command == MESSAGE_GAMEBOARD_UPDATE)
	{
		panel->setTilesState(tiles_state);
	}
	else if(command == MESSAGE_KILLCOMMAND)
	{
		panel->Close();
	}

	if(with_controls)
	{
		actions player_action(panel->get_player_action());
		panel->reset_player_action();

		if(player_action != actions::NONE)
		{
			send_player_action(engine_socket, player_action);
		}
	}

	// Since the gameboard possibly got changed update the mainframe
	// Even if there was no change I don't bother since drawing should not be expensive
	panel->Refresh();
}
 
void wxMainLoop::start()
{
	wxTimer::Start(10);
}


// GuiApp
GuiApp::GuiApp(char in_dim_x, char in_dim_y, tiles &initial_state, bool controls, zmq::socket_t &socket)
	: dim_x(in_dim_x), dim_y(in_dim_y), tiles_state(initial_state), with_controls(controls),
	engine_socket(socket)
{
}

bool GuiApp::OnInit()
{
	// Enable Image Drawing
	wxInitAllImageHandlers();

	// Spawn DrawPanel and display it
	wxFrame *frame = new wxFrame((wxFrame*) NULL, -1, wxT("Bomberman"), wxDefaultPosition, wxSize(800,800));
	DrawPanel *panel = new DrawPanel(frame, dim_x, dim_y, tiles_state);
	frame->Centre();

    frame->Show( true );

	// Start Main Render Loop
	mainloop = new wxMainLoop(dim_x, dim_y, engine_socket, with_controls, panel);
	mainloop->start();
    return true;
}
