//------------------------------------------------
// BOARD.CPP
//------------------------------------------------

#include "board.hpp"

// Libraries
#include <fstream>
#include <algorithm>
#include <chrono>
#include <random>

// CODE

// Methods
void board::load_gameboard(const input_params &game_parameters, player &player_one, player &player_two,
		coordinates *teleporter_pos)
{
	std::fstream input(game_parameters.map, std::fstream::in);
	unsigned dim_x, dim_y;

	if( input.is_open() )
	{
		input >> dim_x >> dim_y;
	}
	state = tiles(dim_y, std::vector<states>(dim_x, states::EMPTY) );

	std::string line;
	unsigned row(0);
	while( input.good())
	{
		getline(input, line);
		if( line.size() == dim_x && row < dim_y)
		{
			for(unsigned col = 0; col < dim_x; ++col)
			{
				if(line[col] - '0' >= 5)
				{
					throw std::string("In the input file there are invalid initial tiles.");
				}
				state[row][col] = static_cast<states>(line[col] - '0');
			}
			++row;
		}
	}
	
	if( row != dim_y)
	{
		throw std::string("The input file does not provide a correct game board");
	}
	else if( _find_players(player_one, player_two) )
	{
		throw std::string("The input file does not provide initial player positions or too many.");
	}
	else if( game_parameters.use_teleporters && _place_teleporters(teleporter_pos, player_one, player_two) )
	{
		throw std::string("The game board does not contain enough space to place power ups.");
	}
	else if( _place_power_ups(game_parameters, player_one, player_two) )
	{
		throw std::string("The game board does not contain enough space to place power ups.");
	}

	input.close();
}

bool board::_find_players(player &player_one, player &player_two)
{
	unsigned player_one_set(0), player_two_set(0);
	for(unsigned row = 0; row < state.size(); ++row)
	{
		for(unsigned col = 0; col < state[0].size(); ++col)
		{
			if(state[row][col] == states::PLAYER_ONE )
			{
				++player_one_set;
				player_one.position = coordinates(col, row);
			}
			if(state[row][col] == states::PLAYER_TWO )
			{
				++player_two_set;
				player_two.position = coordinates(col, row);
			}
		}
	}

	return !( (player_one_set == 1) && (player_two_set == 1) );
}

bool board::_place_teleporters(coordinates *teleporter_pos, player &player_one, player &player_two)
{
	std::vector<coordinates> destructables_near_player_one, destructables_near_player_two;
	unsigned dim_x(state[0].size() ), dim_y(state.size() ),
		 max_distance(player_one.position.distance_to(player_two.position.x, player_two.position.y) / 2 );
	for(unsigned y = 1; y < dim_y - 1; ++y)
	{
		for(unsigned x = 1; x < dim_x - 1; ++x)
		{
			if(state[y][x] != states::DESCTRUCTABLE ||
				(state[y-1][x] != states::DESCTRUCTABLE && state[y-1][x] != states::EMPTY) ||
				(state[y+1][x] != states::DESCTRUCTABLE && state[y+1][x] != states::EMPTY) ||
				(state[y][x-1] != states::DESCTRUCTABLE && state[y][x-1] != states::EMPTY) ||
				(state[y][x+1] != states::DESCTRUCTABLE && state[y][x+1] != states::EMPTY) ) continue;
			
			if(player_one.position.distance_to(x, y) < max_distance)
			{
				destructables_near_player_one.push_back(coordinates(x, y));
			}
			else if(player_two.position.distance_to(x, y) < max_distance)
			{
				destructables_near_player_two.push_back(coordinates(x, y));
			}
		}
	}

	if(destructables_near_player_one.size() < 1 || destructables_near_player_two.size() < 1)
	{
		return 1;
	}

	// instead of picking for random tiles and checking that we did not picked the same tile twice we
	// just shuffle the lists and pick the first tile to place the teleporters on
	auto rng = std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count() );
	std::shuffle(destructables_near_player_one.begin(), destructables_near_player_one.end(), rng );
	std::shuffle(destructables_near_player_two.begin(), destructables_near_player_two.end(), rng );

	state[destructables_near_player_one[0].y][destructables_near_player_one[0].x]
		= states::DESCTRUCTABLE_TELEPORTER;
	teleporter_pos[0].x = destructables_near_player_one[0].x;
	teleporter_pos[0].y = destructables_near_player_one[0].y;
	state[destructables_near_player_two[0].y][destructables_near_player_two[0].x]
		= states::DESCTRUCTABLE_TELEPORTER;
	teleporter_pos[1].x = destructables_near_player_two[0].x;
	teleporter_pos[1].y = destructables_near_player_two[0].y;
	return 0;
}

bool board::_place_power_ups(const input_params &game_parameters, player &player_one, player &player_two)
{
	std::vector<coordinates> destructables_near_player_one, destructables_near_player_two;
	unsigned dim_x(state[0].size() ), dim_y(state.size() ),
		 max_distance(player_one.position.distance_to(player_two.position.x, player_two.position.y) / 2 );
	for(unsigned y = 0; y < dim_y; ++y)
	{
		for(unsigned x = 0; x < dim_x; ++x)
		{
			if(state[y][x] != states::DESCTRUCTABLE) continue;
			
			if(player_one.position.distance_to(x, y) < max_distance)
			{
				destructables_near_player_one.push_back(coordinates(x, y));
			}
			else if(player_two.position.distance_to(x, y) < max_distance)
			{
				destructables_near_player_two.push_back(coordinates(x, y));
			}
		}
	}

	size_t needed_destructable_tiles(game_parameters.use_amount_upgrades +
									2*game_parameters.use_range_upgrades +
									game_parameters.use_speed_upgrades);

	if(destructables_near_player_one.size() < needed_destructable_tiles ||
		destructables_near_player_two.size() < needed_destructable_tiles)
	{
		return 1;
	}

	// instead of picking for random tiles and checking that we did not picked the same tile twice we
	// just shuffle the lists and pick the first tiles to place the powerups on
	auto rng = std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count() );
	std::shuffle(destructables_near_player_one.begin(), destructables_near_player_one.end(), rng );
	std::shuffle(destructables_near_player_two.begin(), destructables_near_player_two.end(), rng );

	if(game_parameters.use_amount_upgrades)
	{
		state[destructables_near_player_one[0].y][destructables_near_player_one[0].x]
			= states::DESCTRUCTABLE_POWERUP_AMOUNT;
		state[destructables_near_player_two[0].y][destructables_near_player_two[0].x]
			= states::DESCTRUCTABLE_POWERUP_AMOUNT;
		destructables_near_player_one.erase(destructables_near_player_one.begin() );
		destructables_near_player_two.erase(destructables_near_player_two.begin() );
	}
	if(game_parameters.use_range_upgrades)
	{
		state[destructables_near_player_one[0].y][destructables_near_player_one[0].x]
			= states::DESCTRUCTABLE_POWERUP_RANGE;
		state[destructables_near_player_two[0].y][destructables_near_player_two[0].x]
			= states::DESCTRUCTABLE_POWERUP_RANGE;
		state[destructables_near_player_one[1].y][destructables_near_player_one[1].x]
			= states::DESCTRUCTABLE_POWERUP_RANGE;
		state[destructables_near_player_two[1].y][destructables_near_player_two[1].x]
			= states::DESCTRUCTABLE_POWERUP_RANGE;
		destructables_near_player_one.erase(destructables_near_player_one.begin(),
				destructables_near_player_one.begin() + 2 );
		destructables_near_player_two.erase(destructables_near_player_two.begin(),
				destructables_near_player_two.begin() + 2 );
	}
	if(game_parameters.use_speed_upgrades)
	{
		state[destructables_near_player_one[0].y][destructables_near_player_one[0].x]
			= states::DESCTRUCTABLE_POWERUP_SPEED;
		state[destructables_near_player_two[0].y][destructables_near_player_two[0].x]
			= states::DESCTRUCTABLE_POWERUP_SPEED;
	}
	return 0;
}

