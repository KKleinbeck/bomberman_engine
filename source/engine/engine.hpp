#pragma once
//------------------------------------------------
//  ENGINE.HPP
//------------------------------------------------

#ifndef ENGINE_HPP
#define ENGINE_HPP

// Libraries
#include <vector>
#include <string>

// My Libraries
#include "defs.hpp"
#include "engine_defs.hpp"
#include "board.hpp"
#include "bomb.hpp"
#include "player.hpp"

// engine : handles, updates and verfies the state of the gameboard
class engine
{
	private:
		board gameboard;
		std::vector<bomb_handler> timed_bombs, triggered_bombs[2];
		player player_one, player_two;
		coordinates teleporter_pos[2];

		rewards player_rewards;

		bool _validate_player_action(actions act, bool player_id);
		int _perform_player_action(actions act, bool player_id);
		int _perform_movement_action(actions act, bool player_id);
		int _perform_bomb_action(actions act, bool player_id);
		void _move_bomb(int x, int y, int x_change, int y_change);

		int _update_bombs();
		int _detonate_bomb(bomb_handler &bomb, int player_id = -1);
		int _detonate_bomb_at(unsigned x, unsigned y, int player_id);

	public:
		engine(const input_params &game_parameters);

		int player_action(actions act, bool player_id);

		int update_game(void);

		tiles get_board(void) {return gameboard.state;}

		unsigned get_height(void) {return gameboard.state.size();}
		unsigned get_width(void) {return gameboard.state[0].size();}

		int get_player_score(bool player_id)
			{return (!player_id ? player_one.get_score() : player_two.get_score() );}
};

#endif
