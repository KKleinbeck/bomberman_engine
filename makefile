CC=g++
PROJECTPATHS=-I./source -I./source/engine -I./source/communication -I./source/gui
EXTERNALPATHS=-I/usr/include/boost_1_62_0/
CFLAGS=-std=c++11 $(PROJECTPATHS) $(EXTERNALPATHS) -g -Wall -c
BOOSTOPTIONS=-lboost_program_options
WXOPTIONS=`wx-config --cxxflags --libs std`
ZMQOPTIONS='-lzmq'

ENGINEDIR = ./source/engine/
ENGINE = $(patsubst $(ENGINEDIR)%.cpp,$(BUILDDIR)%.o,$(wildcard $(ENGINEDIR)*.cpp))
COMMDIR = ./source/communication/
COMM = $(patsubst $(COMMDIR)%.cpp,$(BUILDDIR)%.o,$(wildcard $(COMMDIR)*.cpp))
GUIDIR = ./source/gui/
GUI = $(patsubst $(GUIDIR)%.cpp,$(BUILDDIR)%.o,$(wildcard $(GUIDIR)*.cpp))
BUILDDIR = ./obj/

all: engine gui

engine: dir engine_code communication
	$(CC) -std=c++11 -g -Wall $(ENGINE) $(COMM) -o engine $(ZMQOPTIONS) \
		$(BOOSTOPTIONS)

gui: dir gui_code communication
	$(CC) -std=c++11 -g -Wall $(GUI) $(COMM) -o gui $(WXOPTIONS) $(ZMQOPTIONS) \
		$(BOOSTOPTIONS)

dir:
	mkdir -p $(BUILDDIR)

engine_code: $(ENGINE)

communication: $(COMM)

gui_code: $(GUI)
	
$(ENGINE): $(BUILDDIR)%.o: $(ENGINEDIR)%.cpp
	$(CC) $(CFLAGS) $< -o $@

$(COMM): $(BUILDDIR)%.o: $(COMMDIR)%.cpp
	$(CC) $(CFLAGS) $< -o $@

$(GUI): $(BUILDDIR)%.o: $(GUIDIR)%.cpp
	$(CC) $(CFLAGS) $< -o $@ $(WXOPTIONS)

clean:
	rm -rf $(BUILDDIR)
