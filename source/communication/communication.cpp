//------------------------------------------------
// COMMUNICATION.CPP
//------------------------------------------------

#include "communication.hpp"
#include <unistd.h>

// Libraries

// CODE
void transpose_board(tiles &board)
{
	unsigned dim_y(board.size()), dim_x(board[0].size());
	tiles transposed(dim_x, std::vector<states>(dim_y));
	for(unsigned x = 0; x < dim_x; ++x)
	{
		for(unsigned y = 0; y < dim_y; ++y)
		{
			switch(board[y][x])
			{
				case states::PLAYER_ONE:
					transposed[x][y] = states::PLAYER_TWO;
					break;

				case states::TIMED_BOMB_PLAYER_ONE:
					transposed[x][y] = states::TIMED_BOMB_PLAYER_TWO;
					break;

				case states::TRIGGERED_BOMB_PLAYER_ONE:
					transposed[x][y] = states::TRIGGERED_BOMB_PLAYER_TWO;
					break;

				case states::PLAYER_ONE_ON_TIMED_BOMB:
					transposed[x][y] = states::PLAYER_TWO_ON_TIMED_BOMB;
					break;

				case states::PLAYER_ONE_ON_TRIGGERED_BOMB:
					transposed[x][y] = states::PLAYER_TWO_ON_TRIGGERED_BOMB;
					break;

				case states::PLAYER_TWO:
					transposed[x][y] = states::PLAYER_ONE;
					break;

				case states::TIMED_BOMB_PLAYER_TWO:
					transposed[x][y] = states::TIMED_BOMB_PLAYER_ONE;
					break;

				case states::TRIGGERED_BOMB_PLAYER_TWO:
					transposed[x][y] = states::TRIGGERED_BOMB_PLAYER_ONE;
					break;

				case states::PLAYER_TWO_ON_TIMED_BOMB:
					transposed[x][y] = states::PLAYER_ONE_ON_TIMED_BOMB;
					break;

				case states::PLAYER_TWO_ON_TRIGGERED_BOMB:
					transposed[x][y] = states::PLAYER_ONE_ON_TRIGGERED_BOMB;
					break;

				default:
					transposed[x][y] = board[y][x];
					break;
			}
		}
	}
	board = transposed;
}

bool establish_client_connection(zmq::socket_t &socket, char dim_x, char dim_y, tiles board, bool player_id)
{
	// Give the client 30 second to connect to the server
	socket.setsockopt(ZMQ_RCVTIMEO, 10*1000);
	socket.setsockopt(ZMQ_SNDTIMEO, 10*1000);

	// Prepare the initial message
	char initial_message[3];
	initial_message[0] = MESSAGE_INITIAL_CONTACT;
	initial_message[1] = dim_x;
	initial_message[2] = dim_y;

	// Send the message, hope for an answer
	if(socket.send(zmq::message_t(initial_message, 3)) == false )
	{
		return true;
	}

	zmq::message_t answer;
	if(socket.recv(&answer) == -1)
	{
		return true;
	}
	else
	{
		if(static_cast<char*>(answer.data())[0] != MESSAGE_CONNECTION_ESTABLISHED)
		{
			return true;
		}
	}

	// Upon successfully shaking hands send the client the initial gameboard
	if(send_gameboard(socket, dim_x, dim_y, board, player_id) ) return true;

	// Reset the socket timings and leave
	socket.setsockopt(ZMQ_RCVTIMEO, -1);
	socket.setsockopt(ZMQ_SNDTIMEO, -1);
	return false;
}

bool establish_server_connection(zmq::socket_t &socket, char &dim_x, char &dim_y, tiles &board)
{
	// give the connection 30 seconds to establish a connection
	socket.setsockopt(ZMQ_RCVTIMEO, 30*1000);
	socket.setsockopt(ZMQ_SNDTIMEO, 30*1000);
	
	zmq::message_t board_dimensions;
	if(socket.recv(&board_dimensions) == -1)
	{
		return true;
	}
	else
	{
		if(static_cast<char*>(board_dimensions.data())[0] != MESSAGE_INITIAL_CONTACT
				|| board_dimensions.size() != 3 )
		{
			// Some message recieved, but not the one which should be the first
			// OR data in the wrong format
			return true;
		}
		dim_x = static_cast<char*>(board_dimensions.data())[1];
		dim_y = static_cast<char*>(board_dimensions.data())[2];
	}

	// Upon Successful initial conntect tell the engine of this amazing achievement
	char reply_the_success(MESSAGE_CONNECTION_ESTABLISHED);
	if(socket.send(zmq::message_t(&reply_the_success, 1)) == -1 ) return true;

	// Now create the gameboard with the right size, wait for the engine to reay up and then recieve the board
	board = tiles(dim_y, std::vector<states>(dim_x) );
	usleep(500);
	if(recv_gameboard(socket, board, nullptr) ) return true;

	// Reset the socket timings and leave
	socket.setsockopt(ZMQ_RCVTIMEO, -1);
	socket.setsockopt(ZMQ_SNDTIMEO, -1);
	return false;
}

bool send_gameboard(zmq::socket_t &socket, char dim_x, char dim_y, tiles board, bool player_id)
{
	if(player_id) transpose_board(board);
	char *message = new char[1 + dim_x*dim_y];
	message[0] = MESSAGE_GAMEBOARD_UPDATE;
	int i = 0;
	for(auto &row : board)
	{
		for(auto &tile : row)
		{
			message[1 + i] = static_cast<char>(tile);
			++i;
		}
	}
	if(socket.send(zmq::message_t(message, 1 + dim_x*dim_y), ZMQ_DONTWAIT ) == -1) return true;

	return false;
}

bool recv_gameboard(zmq::socket_t &socket, tiles &board, char *command)
{
	zmq::message_t current_board;
	if(socket.recv(&current_board, ZMQ_DONTWAIT) == -1)
	{
		if(command != nullptr) *command = NOMESSAGE;
		return true;
	}

	if(command != nullptr)
	{
		*command = static_cast<char*>(current_board.data())[0];
	}

	int dim_x(board[0].size()), dim_y(board.size());
	if(static_cast<char*>(current_board.data())[0] == MESSAGE_GAMEBOARD_UPDATE &&
			current_board.size() != 1 + (unsigned)dim_x*(unsigned)dim_y)
	{
		// Recieved some gameboard, but it does not fit the dimension
		return true;
	}

	for(int y = 0; y < dim_y; ++y)
	{
		for(int x = 0; x < dim_x; ++x)
		{
			board[y][x] = static_cast<states>(static_cast<char*>(current_board.data())[1 + x + y*dim_x]);
		}
	}
	return false;
}

bool send_player_action(zmq::socket_t &socket, actions action)
{
	char action_message[2];
	action_message[0] = MESSAGE_PLAYER_ACTION;
	action_message[1] = static_cast<char>(action);

	if(socket.send(zmq::message_t(action_message, 2), ZMQ_DONTWAIT ) == -1) return true;
	return false;
}

char recv_player_action(zmq::socket_t &socket, actions &action, bool player_id)
{
	zmq::message_t player_message;
	int bits_recv(socket.recv(&player_message, ZMQ_DONTWAIT) );
	if(bits_recv == -1)
	{
		return CONNECTION_TERMINATED;
	}
	else if(bits_recv == 0)
	{
		return NOMESSAGE;
	}

	if( bits_recv >= 1 && 
		static_cast<char*>(player_message.data())[1] < 8 &&
		static_cast<char*>(player_message.data())[1] >= 0 )
	{
		action = static_cast<actions>(static_cast<char*>(player_message.data())[1]);
	}

	// Notice, each ai sees itself as player one, i.e., the second input has to be transposed
	if(player_id)
	{
		switch(action)
		{
			case actions::UP:
				action = actions::LEFT;
				break;

			case actions::DOWN:
				action = actions::RIGHT;
				break;

			case actions::RIGHT:
				action = actions::DOWN;
				break;

			case actions::LEFT:
				action = actions::UP;
				break;

			default:
				break;
		}
	}

	return static_cast<char*>(player_message.data())[0];
}
