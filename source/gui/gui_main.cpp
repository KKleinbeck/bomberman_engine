#include <zmq.hpp>

#include "gui.hpp"
#include "communication.hpp"
#include "defs.hpp"
#include "get_gui_args.hpp"
#include <string>

int main(int argc, char* argv[])
{
	unsigned port; // It's default value is given by the command line parser
	bool with_controls(false); 
	if(read_parameters(argc, argv, port, with_controls) )
	{
		return 0;
	}

	// connect to the engine thread
	zmq::context_t context(1);
	zmq::socket_t engine_socket(context, ZMQ_PAIR);

	engine_socket.connect("tcp://localhost:"+std::to_string(port));
	
	// Try to establish connection
	char dim_x, dim_y;
	tiles gameboard;
	if(establish_server_connection(engine_socket, dim_x, dim_y, gameboard)) return -1;

	// startup the rendering frame
	wxApp::SetInstance( new GuiApp(dim_x, dim_y, gameboard, with_controls, engine_socket) );
	wxEntryStart(argc, argv);
	wxTheApp->OnInit();
	wxTheApp->OnRun();

	// Cleanup on exit
	wxTheApp->OnExit();
	wxEntryCleanup();
	return 0;
}

