//------------------------------------------------
//  GET_GUI_ARGS.CPP
//------------------------------------------------

#include "get_gui_args.hpp"

// Libraries

#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "engine_defs.hpp"

// CODE

int read_parameters(int argc, char* argv[], unsigned &port, bool &with_controls)
{
	try
	{
		po::options_description generic_options("Allowed options");
		generic_options.add_options()
			("help,h",
			 	"produce help message")
			("with_controls",
			 	"replaces one AI with a human player")
			("port",
			 	po::value<unsigned>(&port)->default_value(5557),
				"port for communication with the main engine");
			//("replay",
				 //po::value<std::string>(&log_path)->default_value("TIMESTAMP.log"),
				//"name of the log file to replay");
				
		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, generic_options), vm);
		po::notify(vm);

		if(vm.count("help") )
		{
			std::cout << generic_options << std::endl;
			return -1;
		}

		if(vm.count("with_controls") )
		{
			with_controls = true;
		}
		else
		{
			with_controls = false;
		}
	}
	//catch(po::exception& e)
	//{
		//std::std::cerr << "ERROR: " << e.what() << std::endl;
		//return 1;
	//}
	catch(...)
	{
		std::cerr << "Unknown error!" << std::endl;
		return 1;
	}
	return 0;
}
