#pragma once
//------------------------------------------------
//  GET_ENGINE_ARGS.HPP
//------------------------------------------------

#include "engine_defs.hpp"

#ifndef GET_ENGINE_ARGS_HPP
#define GET_ENGINE_ARGS_HPP

int read_parameters(int argc, char* argv[], input_params &game_parameters);

#endif
