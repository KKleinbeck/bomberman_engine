#pragma once
//------------------------------------------------
//  CLIENT.HPP
//------------------------------------------------

#ifndef CLIENT_HPP
#define CLIENT_HPP

// Libraries
#include <zmq.hpp>
#include <unistd.h>
#include <string>

// My Libraries
#include "defs.hpp"
#include "engine_defs.hpp"

// Typedefs and Structures

// Basic function for the interprocess communication
struct client_handle
{
	zmq::socket_t socket;
	FILE* pipe;
	std::string name;
	bool player_id;
	bool is_active;

	client_handle(zmq::context_t &context, std::string in_name, bool in_id, bool in_activity) :
		socket(context, ZMQ_PAIR), name(in_name), player_id(in_id),
		is_active(in_activity) {}

	void open(std::string port, std::string command)
	{
		socket.bind("tcp://*:"+port);
		pipe = popen(command.c_str(), "we");
	}
	void close(void)
	{
		char kill(MESSAGE_KILLCOMMAND);
		socket.send(zmq::message_t(&kill, 1), ZMQ_DONTWAIT);
		pclose(pipe);
	}
};

#endif
