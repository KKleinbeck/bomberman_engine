//------------------------------------------------
//  GET_ENGINE_ARGS.CPP
//------------------------------------------------

#include "get_engine_args.hpp"

// Libraries

#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "engine_defs.hpp"

// CODE
void conflicting_options(const po::variables_map& vm, const char* opt1, const char* opt2)
{
	if(vm.count(opt1) && !vm[opt1].defaulted() && vm.count(opt2) && !vm[opt2].defaulted() )
	{
		throw std::string("Conflicting options.");
	}
}


int read_parameters(int argc, char* argv[], input_params &game_parameters)
{
	std::string config_path, default_config_path("./engine.cfg");
	try
	{
		po::options_description generic_options("Allowed options");
		generic_options.add_options()
			("help,h",
			 	"produce help message")
			("version,v",
			 	"display informations about the version")
			("gui",
			 	"enable the gui")
			("human",
			 	"replaces one AI with a human player")
			//("log,l",				"write the game progression into a log file")
			//("logfile",
				 //po::value<std::string>(&log_path)->default_value("TIMESTAMP.log"), //TODO timestamp
				//"name of the log file");
			("config,c",
			 	po::value<std::string>(&config_path)->default_value(default_config_path),
				"path to a config-file");

		po::options_description general_options("Options valid both from command line and config file");
		general_options.add_options()
			("map,m",
			 	po::value<std::string>(&game_parameters.map)->default_value("./grid.bmd"),
			 	"specify the map to play on")
			("ai1",
			 	po::value<std::string>(&game_parameters.ai1_command),
			 	"specifies the program wo control the first AI")
			("ai2",
			 	po::value<std::string>(&game_parameters.ai2_command),
			 	"specifies the program wo control the second AI")
			("port_ai1",
			 	po::value<std::string>(&game_parameters.port_ai1)->default_value("5555"),
				 "port on the local machine for the first AI")
			("port_ai2",
			 	po::value<std::string>(&game_parameters.port_ai2)->default_value("5556"),
			 	"port on the local machine for the second AI")
			("port_gui",
			 	po::value<std::string>(&game_parameters.port_gui)->default_value("5557"),
				"port on the local machine for the GUI")
			("disable_teleporter",
			 	"disables teleporters for the game")
			("disable_amount",
			 	"disables amount upgrades for the game")
			("disable_range",
			 	"disables range upgrades for the game")
			("disable_speed",
			 	"disables speed upgrades for the game");

		po::options_description config_only("Options valid only in the config file");
		config_only.add_options()
			("reward_moved",
			 	po::value<int>(&game_parameters.player_rewards.moved)->default_value(0),
				"Reward for performing a simple movement")
			("reward_moved_bomb",
			 	po::value<int>(&game_parameters.player_rewards.moved_bomb)->default_value(5),
			 	"Reward for pushing a bomb to a new location")
			("reward_teleported",
			 	po::value<int>(&game_parameters.player_rewards.teleported)->default_value(5),
			 	"Reward for using a teleporter")
			("reward_deploy_timed_bomb",
			 	po::value<int>(&game_parameters.player_rewards.deploy_timed_bomb)->default_value(1),
				"Reward for placing a timed bomb")
			("reward_deploy_triggered_bomb",
			 	po::value<int>(&game_parameters.player_rewards.deploy_triggered_bomb)->default_value(1),
			 	"Reward for placing a triggered bomb")
			("reward_trigger_bomb",
			 	po::value<int>(&game_parameters.player_rewards.trigger_bomb)->default_value(1),
			 	"Reward for triggering bombs")
			("reward_bomb_chain",
			 	po::value<int>(&game_parameters.player_rewards.bomb_chain_explosion)->default_value(5),
			 	"Reward for triggering a chain explosion")
			("reward_destroy_destructible",
			 	po::value<int>(&game_parameters.player_rewards.destroy_destructible)->default_value(10),
			 	"Reward for destroying a destructable tile")
			("reward_pickup_powerup",
			 	po::value<int>(&game_parameters.player_rewards.pickup_powerup)->default_value(30),
			 	"Reward for gathering a powerup")
			("reward_usable_powerup",
			 	po::value<int>(&game_parameters.player_rewards.destroy_usable_powerup)->default_value(-10),
			 	"Reward (or punishment for negative values) for destroying a usable powerup")
			("reward_unusable_powerup",
			 	po::value<int>(&game_parameters.player_rewards.destroy_unusable_powerup)->default_value(30),
			 	"Reward for destroying an unusable powerup")
			("reward_suicide",
			 	po::value<int>(&game_parameters.player_rewards.suicide)->default_value(-100),
			 	"Reward for suiciding")
			("reward_die",
			 	po::value<int>(&game_parameters.player_rewards.die)->default_value(-100),
			 	"Reward for dying")
			("reward_kill_opponent",
			 	po::value<int>(&game_parameters.player_rewards.kill_opponent)->default_value(200),
			 	"Reward for killing the opponent")
			("reward_opponent_sucided",
			 	po::value<int>(&game_parameters.player_rewards.opponent_suicided)->default_value(50),
			 	"Reward if opponent suicides");


		po::options_description cmd_line_options("Options valid from the command line");
		cmd_line_options.add(general_options).add(generic_options);

		po::options_description config_options("Options valid in the config file");
		config_options.add(general_options).add(config_only);

		po::options_description all_options("Full list of all options");
		all_options.add(generic_options).add(general_options).add(config_only);

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, cmd_line_options), vm);
		po::notify(vm);
        
		std::ifstream config_file(config_path.c_str());
		if(!config_file)
		{
			std::cout << "Can not open config file: " << config_path << "\n";
			if (config_path == default_config_path)
			{
				std::cout << "It seems that your default config is missing. "
						<< "I will create an empty version for you.\n";
				std::ofstream oconfig_file(default_config_path.c_str());
				oconfig_file << "#\n"
							<< "# Automatic generated config\n"
							<< "#\n\n"
							<< "# Remove comment on next lines to set the player rewards\n"
							<< "# Values given here are the default values\n"
							<< "# reward_moved = 0"
							<< "# reward_moved_bomb = 1"
							<< "# reward_teleported = 5"
							<< "# reward_deploy_timed_bomb = 1"
							<< "# reward_deploy_triggered_bomb = 1"
							<< "# reward_trigger_bomb = 1"
							<< "# reward_bomb_chain = 5"
							<< "# reward_destroy_destructible = 10"
							<< "# reward_pickup_powerup = 30"
							<< "# reward_usable_powerup = -10"
							<< "# reward_unusable_powerup = 30"
							<< "# reward_suicide = -100"
							<< "# reward_die = -100"
							<< "# reward_kill_opponent = 200"
							<< "# reward_opponent_suicided = 50";
				oconfig_file.close();
			}
			return -1;
		}
		else
		{
			store(parse_config_file(config_file, config_options), vm);
			notify(vm);
		}
		config_file.close();

		conflicting_options(vm, "gui", "human");

		if(vm.count("help") )
		{
			std::cout << all_options << std::endl;
			return -1;
		}
		if(vm.count("version") )
		{
			std::cout << "Beta 0.2.3 --- Latest Commit: Command Line Options" << std::endl;
			return -1;
		}
		if(vm.count("gui") )
		{
			game_parameters.use_gui = true;
			game_parameters.gui_command = "./gui --port " + game_parameters.port_gui;
		}
		if(vm.count("human") )
		{
			game_parameters.use_gui = true;
			game_parameters.human_controls = true;
			game_parameters.gui_command = "./gui --with_controls --port " + game_parameters.port_gui;
		}
		if(vm.count("disable_teleporter") )
		{
			game_parameters.use_teleporters = false;
		}
		if(vm.count("disable_amount") )
		{
			game_parameters.use_amount_upgrades = false;
		}
		if(vm.count("disable_range") )
		{
			game_parameters.use_range_upgrades = false;
		}
		if(vm.count("disable_speed") )
		{
			game_parameters.use_speed_upgrades = false;
		}
	}
	//catch(po::exception& e)
	//{
		//std::std::cerr << "ERROR: " << e.what() << std::endl;
		//return 1;
	//}
	catch(...)
	{
		std::cerr << "Unknown error!" << std::endl;
		return 1;
	}
	return 0;
}
