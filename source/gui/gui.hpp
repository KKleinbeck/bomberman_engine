#pragma once
//------------------------------------------------
//  GUI.HPP
//------------------------------------------------

#ifndef GUI_HPP
#define GUI_HPP

// Libraries
#include <wx/wxprec.h>
#include <wx/sizer.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <zmq.hpp>
#include <vector>

// My Libraries
#include "defs.hpp"

// Typedefs and Structures

// DrawPanel : This draws the gameboard
class DrawPanel : public wxPanel
{
	private:
		char dim_x, dim_y;
		int tile_width, tile_height;
		std::vector<wxImage> preloaded_tiles;

		wxFrame *parent;
		
		tiles tiles_state;
		std::vector<std::vector<wxBitmap>> tiles_bitmap;
		
		actions latest_player_action;
	
	public:
		DrawPanel(wxFrame *in_parent, char in_dim_x, char in_dim_y, tiles initial_state);

		void paintEvent(wxPaintEvent &evt);
		void paintNow();

		void onSize(wxSizeEvent &evt);
		void onKeyDown(wxKeyEvent &evt);

		void render(wxDC& dc);
		void assigneBitmapToTiles(int width, int height);
		void setTilesState(tiles in_state);

		void Close(bool force = false);

		actions get_player_action(void) {return latest_player_action;}
		void reset_player_action(void) {latest_player_action = actions::NONE;}

		DECLARE_EVENT_TABLE()
};

// Main Loop : use a timer to update main frame and communicate mit engine at steady intervals
class wxMainLoop : public wxTimer
{
	private:
		char dim_x, dim_y;
		bool with_controls;

		DrawPanel *panel;

		zmq::socket_t &engine_socket;
		zmq::message_t engine_command;

		tiles tiles_state;

	public:
		wxMainLoop(char in_dim_x, char in_dim_y, zmq::socket_t &socket, bool controls, DrawPanel *in_panel);
		~wxMainLoop() {} // Do not destroy the panel yourself, the GUI is doing that
		void Notify();
		void start();
};


// GuiApp : cunstructs everything around the main frame
class GuiApp: public wxApp
{
	private:
		char dim_x, dim_y;
		tiles tiles_state;
		bool with_controls;

		zmq::socket_t &engine_socket;

		wxMainLoop *mainloop;

	public:
		GuiApp(char in_dim_x, char in_dim_y, tiles &initial_state, bool controls, zmq::socket_t &socket);
		~GuiApp()
		{
			mainloop->Stop();
			wxDELETE(mainloop);

			char kill(MESSAGE_KILLCOMMAND);
			// Setting the lingering time to 100 prevents the program from halting on exectuion
			engine_socket.setsockopt(ZMQ_LINGER, 100);
			engine_socket.send(zmq::message_t(&kill, 1), ZMQ_DONTWAIT );
		}

		virtual bool OnInit();
};

#endif
